import { $, element } from 'protractor';
import { Component, OnInit, Input } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Table } from 'primeng/table';
import { Message, SortEvent } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';
import * as FileSaver from 'file-saver';
import 'jspdf-autotable';
import { SelectItemGroup } from 'primeng/api';
import { FilterService } from 'primeng/api';
import { Meta } from '@angular/platform-browser';
import { DynamicDialogRef } from 'primeng/dynamicdialog';
import { timestamp } from 'rxjs/operators';
import { MenuItem } from 'primeng/api';
import { Console } from 'console';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-filtro-agregation',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.css'],
  styles: [
    `
      :host ::ng-deep .p-dialog .product-image {
        width: 150px;
        margin: 0 auto 2rem auto;
        display: block;
      }
    `,
  ],
  providers: [MessageService, ConfirmationService, FilterService],
})

export class FiltroComponent implements OnInit {
  entities: [];
  bls: [];
  ops: any[];
  legalEntity: any[];
  scenario: any[];
  period: Period[];
  network: any[];
  simulations: any[];
  mode: any[];
  type: any[];
  currency: any[];
  aggregationName: any[];
  copula: any[];

  forms: any[];
  value = 0;

  msgs: Message[];
  msgs1: Message[];
  change;

  /* selectPeriod=[{name:'Clone4'}] */
  selectPeriod: any[];
  selectMode: any[];
  filteredCountries: any[];
  ids = [];
  products: any[];
  ref: DynamicDialogRef;
  createDialog: boolean;
  dialogCorrelation: boolean;
  selectedScenario: any[];
  array;

  datas: any[];
  cols: any[];
  fil: any[];
  datasLista: any[];
  buttons: any[];
  entitys;
  matriz;
  correlation;
  martrizTemporal;
  num;

  items: MenuItem[];
  activeItem: MenuItem;

  data1;
  data2 = [];
  selectedNode;
  selectedType;
  selectedCurrency;
  selectedEntities;
  selectedScenarioName;
  selectedScenarioIds;
  selectedCopula;
  selectedRandomNum;
  selectedRandomBoolean;

  matrizNode;
  selectAgregationNode: any[];
  eventValueNode;

  showIndex;

  simulate;

  Cities: any[] = [
    { label: 'Rome', value: 'ro' },
    { label: 'London', value: 'lo' },
    { label: 'Paris', value: 'pa' },
    { label: 'New York', value: 'ny' },
  ];
  selectedCities: any[];

  elementName;

  /* ------Datos que pasamos por parametros en la etiqueta Component---------- */

  /* @Input() api: string = '';
  @Input() apiList: string = '';
  @Input() controller: string = '';
  @Input() action: string = '';
  @Input() filterCustom: string = '';
  @Input() filter: string = ''; */

  @Input() api: string = 'http://localhost:8080/theanalyticsboutique';
  // @Input() apiLista: string = 'http://localhost:9998/api/datas/loadInit';
  @Input() controller = 'aggregation';
  @Input() action = 'init';

  constructor(
    private http: HttpClient,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private primengConfig: PrimeNGConfig,
    private meta: Meta
  ) {
    var url = this.api + '/' + this.controller + '/' + this.action;
    this.http.get(url).subscribe((res: any) => {
      this.bls = res.bls;
      this.entities = res.entities;
      this.ops = res.ops;
      this.period = res.periods;
      this.network = res.networks;
      this.mode = res.mode;
      this.type = res.type;
      this.currency = res.currencies;
      this.aggregationName = res.aggregationName;
      this.copula = res.copulas;
    });
  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.showIndex = 0;
    this.items = [
      { label: 'a', icon: 'pi pi-fw pi-home' },
      { label: 'b', icon: 'pi pi-fw pi-pencil' },
    ];
    this.activeItem = this.items[0];
    this.correlation = 0.5;

    this.data1 = [
      {
        label: 'TOTAL',
        type: 'person',
        styleClass: 'p-person',
        expanded: true,
        data: { name: 'Role', avatar: 'walter.jpg' },
        children: [],
      },
    ];

    this.selectMode = ['Net'];
    this.selectPeriod = [548];
    this.selectedCurrency = ['EUR'];
    this.selectedCopula = ['Gaussian'];
    this.selectedRandomNum = 0;
  }
  changePeriodOptions(event) {
    console.log(event);
  }

  changee(event) {
    console.log(event);
    let newId;

    for (let index = 0; index < event.value.length; index++) {
      const element = event.value[index];
      newId = element.id;
      this.ids.push(newId.toString());
    }
    this.selectedEntities = this.ids;

    console.log(this.ids);
    let url =
      this.api +
      '/' +
      this.controller +
      '/' +
      'loadOrganizationLegal?entities=' +
      this.ids.toString();
    this.http.get(url).subscribe((res: any) => {
      this.legalEntity = res;
    });

    let params = {
      period: '545',
      type: 'Net',
      entities: this.ids,
    };
    let urls =
      this.api +
      '/' +
      this.controller +
      '/' +
      'loadCorrelation/?obj=' +
      JSON.stringify(params);

    this.http.get(urls).subscribe((res: any) => {
      this.scenario = res;
    });
    this.ids = [];
  }
  filterCountry(event) {
    let filtered: any[] = [];
    let query = event.query;
    for (let i = 0; i < this.currency.length; i++) {
      let country = this.currency[i];
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(country);
      }
    }

    this.filteredCountries = filtered;
  }

  selectProduct(product: any) {
    this.ref.close(product);
  }
  show() {
    this.createDialog = true;
    let newDate = new Date();
    let timeStam = newDate.getTime().toString();
    console.log('este es el timestamp: ' + newDate.getTime());

    let urlSimulation =
      this.api +
      '/' +
      'calculation' +
      '/' +
      'getSimulations/?foobar=' +
      timeStam +
      '&uid=241';

    this.http.get(urlSimulation).subscribe((res: any) => {
      console.log('esta es la respuesta ' + res);
      this.simulations = res;
    });
  }

  showCorrelation(param: String) {
    console.log(param);

    if (param == 'agregation') {
      console.log(this.matrizNode);

      this.matriz = this.matrizNode;
      this.martrizTemporal = JSON.parse(JSON.stringify(this.matriz));
      this.dialogCorrelation = true;
    } else {
      console.log('entro en filter');
      this.martrizTemporal = JSON.parse(JSON.stringify(this.matriz));
      this.dialogCorrelation = true;
    }
  }

  ngOnDestroy() {
    if (this.createDialog) {
      this.createDialog = false;
    }
  }

  changeScenario(event) {
    /*     this.matriz = {};
    let contador = 0;

    for (var i = 0; i < this.selectedScenario.length; i++) {
      for (var j = 0; j < this.selectedScenario.length; j++) {
        if (this.matriz[i] == undefined) {
          console.log('entro 1');
          this.matriz[i] = {};
        }

        if (this.matriz[i][j] == undefined) {
          console.log('entro 2');
          this.matriz[i][j] = {};
        }

        if (i == j) {
          this.matriz[i][j] = 1;
        } else {
          this.matriz[i][j] = this.correlation;
        }
      }

      console.log(this.matriz);
      console.log(this.selectedScenario);
    } */
    this.matriz = [];
    this.selectedScenarioName = [];
    this.selectedScenarioIds = [];
    console.log(event.value);

    for (var j = 0; j < this.selectedScenario.length; j++) {
      this.matriz.push([]);
      this.selectedScenarioName.push(event.value[j].name);
      this.selectedScenarioIds.push(event.value[j].id);
      for (let i = 0; i < this.selectedScenario.length; i++) {
        if (i == j) {
          this.matriz[j].push(1);
        } else {
          this.matriz[j].push(this.correlation);
        }
      }
    }
  }
  changeNodeAgregation(event) {
    console.log(event);
    this.eventValueNode = event.value;

    this.matrizNode = [];

    for (var i = 0; i < this.selectAgregationNode.length; i++) {}
    for (var j = 0; j < this.selectAgregationNode.length; j++) {
      this.matrizNode.push([]);
      for (let i = 0; i < this.selectAgregationNode.length; i++) {
        if (i == j) {
          this.matrizNode[j].push(1);
        } else {
          this.matrizNode[j].push(0.5);
        }
      }
    }
  }
  changeCorrelation(event) {
    for (var i = 0; i < this.matriz.length; i++) {
      for (var j = 0; j < this.matriz.length; j++) {
        if (i == j) {
          this.matriz[i][j] = 1;
        } else {
          this.matriz[i][j] = event.value;
        }
      }
    }
  }

  cancelCorrelation() {
    console.log('esto es la matriz' + this.matriz);
    console.log('esto es la temporal');

    this.matriz = this.martrizTemporal;
    //delete this.martrizTemporal;
    this.dialogCorrelation = false;
  }
  saveCorrelation() {
    this.dialogCorrelation = false;

    /* this.matriz[index]=this.itemCloned[item];
    delete this.itemCloned[item] */
  }
  changeCell(event, i, j, item) {
    this.matriz[j][i] = parseFloat(item[i][j]);
    console.log(this.matriz);
    console.log(this.martrizTemporal);
  }

  onNodeSelect(event) {
    this.messageService.add({
      severity: 'success',
      summary: 'Node Selected',
      detail: event.node.label,
    });
  }

  createNode() {
    let params = [];

    for (let index = 0; index < this.data1.length; index++) {
      const element = this.data1[index];
      for (let index = 0; index < this.eventValueNode.length; index++) {
        const elementNode = this.eventValueNode[index];

        const nodo = {
          label: `${elementNode.name}`,
          type: 'person',
          styleClass: 'p-person',
          expanded: true,
          data: { name: `${elementNode.role}`, avatar: 'saul.jpg' },
        };

        params.push(nodo);
      }
      element.children = params;
    }

    //this.data2 = this.data1;
    this.data2 = JSON.parse(JSON.stringify(this.data1));

    console.log('sale');
    console.log(this.data2);
  }

  selectTabView(e) {
    this.showIndex = e.index;
    console.log(this.showIndex);
  }

  onClickSimulate() {
    console.log('entra');
    console.log(this.selectedCopula);
    this.simulate = {
      options: {
        numberOfSimulations: 5000000,
        reduceResultsTo: 100000,
        objetives: [
          0.25, 0.5, 0.75, 0.8, 0.95, 0.975, 0.99, 0.995, 0.999, 0.9995,
        ],
        conditional: [0.99, 0.999],
      },
      scenario: {
        level1: {
          correlation: {
            mode: 'sorted',
            node: 'No',
            currency: this.selectedCurrency[0],
            networkId: '',
            randomSeed: this.selectedRandomNum,
            randomAutomatic: this.selectedRandomBoolean,
            type: this.selectMode[0],
            period: this.selectPeriod[0],
            entities: this.selectedEntities,
            scenarios: this.selectedScenarioIds,
            cells: this.selectedScenarioName,
            copula: this.selectedCopula[0],
            utilization: [1, 1],
            exchangeRatio: [1, 1],
            matrix: this.matriz,
          },
        },
      },
    };
    for (let i = 0; i < this.scenario.length; i++) {
      const itemScenario = this.scenario[i];
      for (let j = 0; j < this.selectedScenarioIds.length; j++) {
        const itemIds = this.selectedScenarioIds[j];
        if (itemScenario.id === itemIds) {
          let gross = itemScenario.distributions.gross;
          let net = itemScenario.distributions.net;
          let seed = itemScenario.seed;
          this.elementName = this.selectedScenarioName[j];

          this.simulate.scenario.level1[this.elementName] = {};
          this.simulate.scenario.level1[this.elementName] = {
            gross: gross,
            net: net,
            seed: seed,
          };

          /*           this.simulate.scenario.level1 = {
            [this.elementName]: {
              gross: gross,
              net: net,
              seed: seed,
            },
          }; */

          //this.simulate.scenario.level1.push(obj);
        }
      }
    }
    console.log(this.simulate);
    console.log(JSON.stringify(this.simulate));
  }

  changeCurrency(event) {
    console.log(this.selectedCurrency);
  }
  onClickClear() {
    this.data2 = [];
  }

  onChangeRandomBoolean(event) {
    console.log(event.checked);
    console.log(this.selectedRandomBoolean);
    if (this.selectedRandomBoolean) {
          function getDateTimeToString() {
      var date = new Date();

      return (
        date.getDate() +
        '' +
        (date.getMonth() + 1) +
        '' +
        date.getFullYear() +
        '' +
        date.getHours() +
        '' +
        date.getMinutes() +
        '' +
        date.getSeconds()
      );
    }

    function getRandomAutomatic() {
      var date = getDateTimeToString();

      return parseInt(date);
    }
    this.selectedRandomNum=getRandomAutomatic();
      
    }else{
      this.selectedRandomNum=0;
    }


    
  }
}

interface Period {
  name: string;
  id: number;
  class?: string;
  frozenPeriod?: string;
  periodDate?: string;
  periodType?: string;
}
