import { NgModule,CUSTOM_ELEMENTS_SCHEMA, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TableModule } from 'primeng/table';
import {HttpClientModule} from '@angular/common/http';
import {ButtonModule} from 'primeng/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FiltroComponent } from './componente/filtro/filtro.component';
import { SharedModule } from 'primeng/api';
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {TooltipModule} from 'primeng/tooltip';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {KeyFilterModule} from 'primeng/keyfilter';
import { FormsModule } from '@angular/forms';
import {DialogModule} from 'primeng/dialog';
import {DividerModule} from 'primeng/divider';
import {RippleModule} from 'primeng/ripple';
import {ToolbarModule} from 'primeng/toolbar';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import {CalendarModule} from 'primeng/calendar';
import {DropdownModule} from 'primeng/dropdown';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {MultiSelectModule} from 'primeng/multiselect';
import {CheckboxModule} from 'primeng/checkbox';
import {TriStateCheckboxModule} from 'primeng/tristatecheckbox';
import { createCustomElement } from '@angular/elements';
import 'web-component-essentials';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import {PasswordModule} from 'primeng/password';
import {FileUploadModule} from 'primeng/fileupload';
import {InputNumberModule} from 'primeng/inputnumber';
import {ListboxModule} from 'primeng/listbox';
import {TabMenuModule} from 'primeng/tabmenu';
import {TabViewModule} from 'primeng/tabview';
import {OrganizationChartModule} from 'primeng/organizationchart';
import {CardModule} from 'primeng/card';

@NgModule({
  declarations: [
    FiltroComponent,
    
  ],
  imports: [
    BrowserModule,
    TableModule,
    SharedModule,
    MessagesModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AccordionModule,
    HttpClientModule,
    TooltipModule,
    ButtonModule,
    KeyFilterModule,
    FormsModule,
    DialogModule,
    DividerModule,
    RippleModule,
    ToolbarModule,
    DynamicDialogModule,
    ConfirmDialogModule,
    MessageModule,
    ToastModule, 
    CalendarModule,
    InputTextModule,
    DropdownModule,
    AutoCompleteModule,
    MultiSelectModule,
    CheckboxModule,
    TriStateCheckboxModule,
    ReactiveFormsModule,
    InputTextareaModule,
    PasswordModule,
    FileUploadModule,
    InputNumberModule,
    ListboxModule,
    TabMenuModule,
    TabViewModule,
    OrganizationChartModule,
    CardModule
    
       
  ],
  providers: [],
  entryComponents: [FiltroComponent],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { 

  constructor(private injector:Injector){
    const el= createCustomElement(FiltroComponent,{injector});
    customElements.define('app-filtro-agregation',el);

  }

  ngDoBootstrap(){

  }

}
